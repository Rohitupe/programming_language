from django.urls import path
from . import views

urlpatterns = [
    path('', views.desp_home, name='description_home'),
]
