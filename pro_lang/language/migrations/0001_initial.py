# Generated by Django 3.0.2 on 2020-01-19 02:10

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('home', '0002_auto_20200118_1121'),
    ]

    operations = [
        migrations.CreateModel(
            name='Description',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url_link', models.URLField(max_length=1000)),
                ('image', models.ImageField(upload_to='media')),
                ('desp', models.TextField()),
                ('salary', models.IntegerField()),
                ('name', models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, to='home.HomeLanguage')),
            ],
        ),
    ]
