from django.db import models
from home.models import HomeLanguage

# Create your models here.


class Description(models.Model):
    name = models.ForeignKey(HomeLanguage, default=None,
                             on_delete=models.CASCADE)
    url_link = models.URLField(max_length=1000)
    image = models.ImageField(upload_to='images/')
    desp = models.TextField()
    salary = models.IntegerField()

    def __str__(self):
        return f'{str(self.name)} + {self.salary}'
