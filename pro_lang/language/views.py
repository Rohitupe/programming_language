from django.shortcuts import render
from . models import Description
# Create your views here.


def desp_home(request):
    obj = Description.objects.all()

    context = {
        'objects': obj
    }
    return render(request, 'language/descriptive.html', context)
