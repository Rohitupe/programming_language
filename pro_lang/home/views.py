from django.shortcuts import render, redirect, get_object_or_404
from .models import HomeLanguage
from language.models import Description
from django.contrib import messages
from django.db.models import Q
# Create your views here.
from django.http import Http404


def index(request):
    data = HomeLanguage.objects.all()
    context = {
        'data': data
    }
    return render(request, 'home/content.html', context)


def search(request):
    if request.method == 'POST':
        search_bar = request.POST['search']
        print(search_bar)
        search_content = search_bar.title()
        if search_content:
            matching = HomeLanguage.objects.filter(
                Q(name__icontains=search_content))
            if matching:
                return render(request, 'home/content.html', {'search_result': matching})
            else:
                messages.warning(request, 'Not Found')
                return redirect('index')
        else:
            messages.warning(request, 'Not Found')
            return redirect('index')
    return render(request, 'home/content.html')


def detail_page(request, pk):

    instance = get_object_or_404(HomeLanguage,
                                 pk=pk)
    print(instance.pk)
    context = {
        'instance': instance
    }

    return render(request, 'home/detail.html', context)
