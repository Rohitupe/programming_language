from django.urls import path
from . import views
from language.views import desp_home

urlpatterns = [
    path('', views.index, name="index"),
    path('<int:pk>/', views.detail_page, name="detail"),
    path('search/', views.search, name="search"),
]
