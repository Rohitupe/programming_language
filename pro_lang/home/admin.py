from django.contrib import admin
from home.models import HomeLanguage

# Register your models here.
admin.site.register(HomeLanguage)
