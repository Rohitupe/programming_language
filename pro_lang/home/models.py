from django.db import models

from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.


class HomeLanguage(models.Model):
    name = models.CharField(max_length=500)
    diff_level = models.IntegerField(
        default=1,
        validators=[MaxValueValidator(5), MinValueValidator(1)]
    )

    def __str__(self):
        return f'{self.name}'

